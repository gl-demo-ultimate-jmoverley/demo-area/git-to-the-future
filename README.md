# Git to the future
### Why

This project and repo is to demonstrate that git commit history timestamps are not verified :)
Check out the [commits](https://gitlab.com/gl-demo-ultimate-jmoverley/demo-area/git-to-the-future/-/commits/main?ref_type=heads) history, great scott!

This is important to note, and careful correlation needs to be considered when looking at user events audit records from the api (push events + commit info)
NB: the user contribution events are server timestamped - and more reliable for relative time context! https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events 

### Files 
- [script.txt](https://gitlab.com/gl-demo-ultimate-jmoverley/demo-area/git-to-the-future/-/blob/main/script.txt): File being edited as example

### Details
This journey was achieve by using a virtual machine instance with *nix (ubuntu), changing the system time within the VM, then updating the script.txt file before making a git commit. 

The following dates were used:

1. Disable time continuum safety switches before attempting ant temporal travel
```
sudo timedatectl set-ntp false
git config http.sslVerify "false"
```
	
2. BTTF 1 start - 1st dept Einstine's fine! see..
```
sudo date --set "25 Oct 1985 01:21:00"
```

3. BTTF 1 fleeing terrorists
```
sudo date --set "25 Oct 1985 01:35:00"
```

4. BTTF2 - to future - marty your kids
```
sudo date --set "21 Oct 2015 16:29:00"
```

5. BTTF2 - something wrong!
```
sudo date --set "27 Oct 1985 02:42:00"
```


Post experiment return normality to the time continuum with:
```
sudo timedatectl set-ntp true
```

<Dr. Scott>



